<h1 align="center" name="pywebio-en">NodeBB post to Mastodon</h1>
<p align="center">
    <em>created with Python</em>
</p>
<p align="center">
    <a href="https://pylint.org/">
        <img src="https://img.shields.io/badge/linting-pylint-yellowgreen" alt="Linting">
    </a>
    <a href="https://pypi.org/project/flake8/">
        <img src="https://img.shields.io/badge/linting-flake8-yellowgreen" alt="Linting">
    </a>
    <a href="https://www.python.org/">
        <img src="https://img.shields.io/badge/python->%3D%203.10.1-brightgreen" alt="Python Version">
    </a>
    <a href="https://www.gnu.org/licenses/gpl-3.0.en.html">
        <img src="https://img.shields.io/gitlab/license/Bullet64/restic_ui_pywebio?color=yellow" alt="License">
    </a>
</p>


# nodebb_post_to_mastodon

## About

I needed the ability to post a new post to Mastodon in my NodeBB forum.
It is configurable in the configuration which user of the NodeBB can post (UID)

This Python script will be started by a crontab job.
Crontab will execute the shell script nodebb_post_to_mastodon.sh every minute.
When the shell script starts the Python file main.py, it will check for a new post.
If a new post is found, it will send the post to Mastodon.

This project is quite far from perfect, but it helped me a lot to understand the world of Python, PyCharm and git a little.

If it helps someone along the way as well, I'm glad. For questions and suggestions you can write me in my [forum](https://linux-nerds.org/)

## Dependencies

* python >= 3.10
* pipenv
* git

## Installation

### Clone Repository

    git clone https://gitlab.com/Bullet64/nodebb_post_to_mastodon.git

### Create env

    cd /home/<USER>/nodebb_post_to_mastodon

    # We upgrade to last pip version
    python -m pip install -U pip
    pip install pipenv

    pipenv --python 3.11
    pipenv shell

## Usage

   Create crontab for <USER>

    crontab -u <USER> -e

   This script is triggered by a crontab job, which runs every minute.
   Line 2 is for debugging purposes by logging the output.

    # m h  dom mon dow   command
    #* * * * * /home/<USER>/nodebb_post_to_mastodon/./nodebb_post_to_mastodon.sh >> /home/user_nodebb/nodebb_post_to_mastodon/cron_log.log 2>&1
    * * * * * /home/<USER>/nodebb_post_to_mastodon/./nodebb_post_to_mastodon.sh

## Tested on

- Debian Bookworm 12

## Known problems

    None at present

![Example Post from Mastodon](images/example.png "example post")
