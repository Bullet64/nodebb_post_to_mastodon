###############################################
# Imports
###############################################
import os
import redis
import requests
from dotenv import load_dotenv


###############################################
# Settings
###############################################
# Read settings from file .env
load_dotenv()

# Fetching Redis configuration values from environment variables
REDIS_HOST = os.getenv("REDIS_HOST")
REDIS_PORT = os.getenv("REDIS_PORT")
REDIS_DB = os.getenv("REDIS_DB")
REDIS_USERNAME = os.getenv("REDIS_USERNAME")
REDIS_PASSWORD = os.getenv("REDIS_PASSWORD")

# Load Mastodon
MASTODON_TOKEN = os.getenv("MASTODON_TOKEN")
MASTODON_URL  = os.getenv("MASTODON_URL")

# Load NodeBB
ALLOWED_USER = os.getenv("ALLOWED_USER", "").split(',')
DISALLOWED_CATEGORIES = os.getenv("DISALLOWED_CATEGORIES")

URL = os.getenv("URL")

###############################################
# Redis connection
###############################################
def connect_redis():
    # When I need another db as 0 the redis user must have permission SELECT in redis.
    # My user don't have this permission, so I set db to 0
    # When I use REDIS_DB I got an error for missing SELECT permission!
    try:
        # Connect to the Redis database with authentication
        redis_connection = redis.Redis(host=REDIS_HOST,
                                       port=REDIS_PORT,
                                       db= 0,
                                       username=REDIS_USERNAME,
                                       password=REDIS_PASSWORD)
    except redis.exceptions.RedisError as e:
        print('Error connecting to Redis:', e)
        return None
    return redis_connection


###############################################
# Get last_topic_id from redis db
###############################################

def get_last_topic_id():
    r = connect_redis()

    # Get the last Topic ID from the sorted set
    last_topic_id = r.zrange('topics:tid', -1, -1, withscores=True)
    # we get [(b'1468', 1690834233147.0)]

    if last_topic_id:
        last_topic_id = int(last_topic_id[0][0].decode())
        return last_topic_id
    else:
        print('No Topics found in the sorted set.')
        return None


###############################################
# Get last_topic_uid from redis db
###############################################

def get_last_topic_uid():
    r = connect_redis()

    # Get the last Topic ID from the sorted set
    last_topic_id = r.zrange('topics:tid', -1, -1, withscores=True)

    if last_topic_id:
        last_topic_id = int(last_topic_id[0][0].decode())

        # Get the topic data using the last_topic_id
        topic_data = r.hgetall(f'topic:{last_topic_id}')

        # Extract the 'uid' from the topic_data dictionary
        uid = int(topic_data.get(b'uid', -1).decode())

        if uid != -1:
            return uid
        else:
            print('UID not found in the topic data.')
            return None
    else:
        print('No Topics found in the sorted set.')
        return None


###############################################
# Get last_topic_cid from redis db
###############################################

def get_last_topic_cid():
    # Get the last Topic Category ID (cid) from the sorted set
    r = connect_redis()

    last_topic_id = r.zrange('topics:tid', -1, -1, withscores=True)

    if last_topic_id:
        last_topic_id = int(last_topic_id[0][0].decode())

        # Get the topic data using the last_topic_id
        topic_data = r.hgetall(f'topic:{last_topic_id}')

        # Extract the 'cid' from the topic_data dictionary
        cid = int(topic_data.get(b'cid', -1).decode())

        if cid != -1:
            return cid
        else:
            print('CID not found in the topic data.')
            return None
    else:
        print('No Topics found in the sorted set.')
        return None


###############################################
# Get last_topic_slug from redis db
###############################################

def get_last_topic_slug():
    r = connect_redis()

    # Get the last Topic ID from the sorted set
    last_topic_id = r.zrange('topics:tid', -1, -1, withscores=True)

    if last_topic_id:
        last_topic_id = int(last_topic_id[0][0].decode())

        # Get the topic data using the last_topic_id
        topic_data = r.hgetall(f'topic:{last_topic_id}')

        # Extract the 'uid' from the topic_data dictionary
        slug = topic_data.get(b'slug', b'').decode()

        if slug:
            return slug
        else:
            print('Slug not found in the topic data.')
            return None
    else:
        print('No Topics found in the sorted set.')
        return None


###############################################
# Get topic_tags from redis db
###############################################

def get_topic_tags():
    # Get the last Topic ID from the sorted set
    r = connect_redis()

    last_topic_id = r.zrange('topics:tid', -1, -1, withscores=True)

    if last_topic_id:
        last_topic_id = int(last_topic_id[0][0].decode())
        try:
            # Get the topic data using the last_topic_id
            topic_data = r.hgetall(f'topic:{last_topic_id}')
            # print("Topic_Data", topic_data)

            # Extract the 'tags' from the topic_data dictionary
            tags = topic_data.get(b'tags', -1).decode()

            tags_single = tags.split(",")

            tags = []

            for index, element in enumerate(tags_single):
                # print(index, ":", element)
                tags.append(f"#{element} ")

            # Result ['#ansible', '#linux'] Build string
            tags_list = ''
            tags_string = tags_list.join(tags)
            #print(tags_string)

            # Extract the 'uid' from the topic_data dictionary
            # uid = int(topic_data.get(b'uid', -1).decode())

            return tags_string

        except Exception as e:
            print("NO Tags!: ", e)
    else:
        print('No Topics found in the sorted set.')
        return None


###############################################
# Check UID if allowed to post to mastodon
###############################################

def check_uid():
    # get_topic_uid
    uid = get_last_topic_uid()
    print(f'User ID of the topic creator: {uid}')

    for item in ALLOWED_USER:
        if str(uid) in item:
            # print("Success",item)
            return True
        else:
            print('Error')
            return False


###############################################
# Check CID if disallowed to post to mastodon
###############################################

def check_cid(cid):
    for item in DISALLOWED_CATEGORIES:
        if str(cid) in item:
            # print("Success",item)
            return '1'
        else:
            print('Error')
            return None


###############################################
# Main
###############################################

# Example usage

# get_last_topic_id
last_topic_id = get_last_topic_id()
print('Last Topic ID:', last_topic_id)

# get_last_topic_slug
title = get_last_topic_slug()
print("Title:", title)

# get_topic_tags
tags = get_topic_tags()
print("Tags", tags)

# get_topic_cid - Categories ID
cid = get_last_topic_cid()
print("CID:", cid)

# CID 13 - disallowed
if get_last_topic_cid() == 13:
    # I have private categories that I don't post to Mastodon.
    print("Disallowed CID")
else:

    if check_uid():  # True - allowed to post

        # read second_last_topic_id from file
        with open("last_topic_id.txt", "r") as f:
            data = f.readline()
            f.close()

        # check for new topic
        last_topic_stored = int(data.strip())

        if last_topic_id > int(last_topic_stored):

            try:
                if tags is None:
                    # Construct the status parameter by concatenating the title and URL
                    status_parameter = f"Ein neuer Forumbeitrag: {URL}/{title} {title}"
                else:
                    # Construct the status parameter by concatenating the title and URL
                    status_parameter = f"Ein neuer Forumbeitrag: {URL}/{title} {title} {tags}"

                # Headers with the bearer token
                headers = {
                           'Authorization': f'Bearer {MASTODON_TOKEN}'
                          }

                # Data for the POST request
                data = {
                        'status': status_parameter
                       }

                response = None
                try:
                    # Send the POST request using requests
                    response = requests.post(MASTODON_URL,
                                             headers=headers,
                                             data=data)

                    # Check if the request was successful (status code 200)
                    if not response.ok:
                        print(f'Request failed with status code: {response.status_code}')
                        print(f'Error response: {response.text}')

                except requests.exceptions.RequestException as e:
                    print(f'Request failed: {e}')

                else:
                    print('Request successful!')

            except Exception as e:
                print('An unexpected error occurred:', e)

        # write new last_topic_id to file
        with open("last_topic_id.txt", "w") as f:
            f.write(str(last_topic_id))

    else:
        print("Disallowed to post")

